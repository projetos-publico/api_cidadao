# Documentação da API - Cidadão #

API em PHP para manipular dados de cidadão.

**Project license**   
[![GitHub](https://img.shields.io/github/license/fabianobasso/Jogo_De_Xadrez)](https://github.com/fabianobasso/api_send_email/blob/master/LICENSE) 

---

### Dependência da API: ###

* PHP >= 7.4
* Composer
* SlimFramework 3.0
* MariaDb 10.5 

---



### Considerações: ###
* Motivo de usar o **SlimFramework**, Escolhi usar porque ele é framework modularizado com uma estrutura inicial simples comparada com os frameworks convencionais. Além de ser bem leve, possui como principal característica a implementação de API REST permitindo criar API seguras e robustas, com baixa complexidade.

* Motivo de usar PHP >= 7.4, para utilizar as novas features da linguagem.

* Motivo de usar Composer, gerenciar as dependências de pacotes do projeto.

* Motivo de usar MariaDB, É Open Source e gratuito, independente se for para uso comercial ou pessoal.

---



### Configuração:  ###
1 -  Renomear o arquivo **api_cidadao_desafio/env.example.php** para **env.php**, e editar o arquivo com as configurações do banco de dados. 
```php
// Configuração do Banco de Dados
putenv('DB_CONFIG_HOST='); // Host do banco de dados
putenv('DB_CONFIG_DBNAME='); // Nome do banco de dados
putenv('DB_CONFIG_USER='); // Usuário de acesso
putenv('DB_CONFIG_PASSWORD='); // Senha do usuário
putenv('DB_CONFIG_PORT='); // Porta do banco de dados
```
2 - Baixar o [Composer](https://getcomposer.org/download/) na pasta **api_cidadao_desafio/** e executar o comando para instalar as dependências do projeto.

```bash
php composer.phar update
```
---



### Iniciar o projeto com Docker ou usando Php:  ###



1 - **Usando Docker**, é necessário ter o docker e docker-compose instalado e configurado, executar o camando na pasta raiz onde está o arquivo **docker-compose.yml**

```bash
docker-compose up -d
```

OBS: será configurado de forma automática e rápido com imagem ubuntu:20.04 oficial customizada do meu repositório do docker hub com o php8.0 e o mariadb:10.5 oficial.
Para esta opção de execução usar essas configurações no arquivo **env.php**, que foi explicado como configurar anteriormente:
```php
putenv('DB_CONFIG_HOST=db');
putenv('DB_CONFIG_DBNAME=api_cidadao');
putenv('DB_CONFIG_USER=root');
putenv('DB_CONFIG_PASSWORD=dev@2021!@#$');
putenv('DB_CONFIG_PORT=3306');
```

```bash
# A api vai estar configurada em
http://localhost:9000
```



2 - **Usando php**, é necessário ter php >= 7.4 instalado e configurado, mariadb instalado e configurado.

* Rodar o script **/api_cidadao_desafio/DB/schema.sql** para configurar o banco de dados;

* Executar o comando para inciar a api;
```bash
php -S localhost:9000 -t /api_cidadao_desafio/public/
```

OBS: configurar o arquivo **env.php** com as configurações do seu banco.

---



## Como consumir API ##



### Rota GET  http://localhost:9000/cidadao ###
Essa rota mostra as informações do cidadão cadastrado, se for enviado um json com o cpf vai fazer a busca por um cpf e retornar somente o cidadão com o cpf cadastrado, se não for passado  o parâmetro vai trazer todos os cidadão cadastrado.

![Get All](./Testes/GetALL.png)
Mostra todos cidadãos.

![Get One](./Testes/GetOne.png)
Busca por CPF


---


### Rota POST http://localhost:9000/cidadao ###
Essa rota faz o cadastro de um cidadão enviar um json com os dados
```json
{
	"cpf": "000.000.000-00",
	"nome": "Nome",
	"sobrenome": "Sobrenome",
	"email": "email",
	"telefone": "00-00000000",
	"cep": "00000-000"
}
```
Todos os campos são obrigatorio nenhum pode ser vazio. 

![Save](./Testes/Save.png)
Salvar cidadão na base de dados.

![Save](./Testes/SaveExist.png)
Não permite cadastrar cpf igual.

---

### Rota PUT http://localhost:9000/cidadao ###
Essa rota atualiza os dados de um cidadão cadastrado, Obrigatório informar o cpf que deseja atualizar, os demais campos são opcionais precisa mandar apenas o campo que quer atualizar:
```json
{
	"cpf": "000.000.000-00",
	"cep": "11111-111"
}
```
![Save](./Testes/update.png)
Nesse exemplo foi atualizado somente o endereço


---

### Rota DELETE http://localhost:9000/cidadao ###
Essa rota é para deletar um cidadão cadastrado, obrigatório informar o cpf do cidadão a ser deletado
```json
{
	"cpf": "000.000.000-00"
}
```

![Save](./Testes/Delete.png)
Deletando um cidadão.


### Validações de dados ###
![Save](./Testes/Validacao.png)
Valida os campos, CPF verifica se é válido para cadastro e CEP verifica se é válido e existe para preencher o logradouro pela API ViaCEP.



## Consumindo API por linha de comando ##
### Usando o curl ###
* -i, --include	Mostra o header da resposta no output
* -d, --data	Dados a serem enviados no POST
* -H, --header	Envia o header da requisição
* -X, --request	Especifica o método HTTP a ser usado na requisição

![Save](./Testes/getAllLine.png)

**curl -i http://localhost:9000/cidadao**

---

![Save](./Testes/UpdateLine.png)

**curl -i -X PUT -d "cpf=880.419.920-21&cep=58400-752" http://localhost:9000/cidadao**


## Author

**Fabiano Basso Antonio**

I also work as a **FULL STACK DEVELOPER** freelancer, if you have a project, this is the right time to get it off the ground, we can talk, and I'll present the best solution for you.


## My contacts:
**Portfólio:** https://fabianobasso.github.io/

**Linkedin:** https://www.linkedin.com/in/fabiano-basso

**Whatsapp:** [Send Message](https://api.whatsapp.com/send?phone=5519999979098)

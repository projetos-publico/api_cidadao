<?php

namespace App\DAO;

use PDO;

class Connection
{

    /**
     * 
     * @var \PDO
     */
    protected $pdo;

    public function __construct()
    {
        $host = getenv('DB_CONFIG_HOST');
        $port = getenv('DB_CONFIG_PORT');
        $user = getenv('DB_CONFIG_USER');
        $pass = getenv('DB_CONFIG_PASSWORD');
        $dbname = getenv('DB_CONFIG_DBNAME');

        $dsn = "mysql:host={$host};dbname={$dbname};port={$port}";

        try {

            $this->pdo = new \PDO($dsn, $user, $pass);
            $this->pdo->setAttribute(
                \PDO::ATTR_ERRMODE,
                \PDO::ERRMODE_EXCEPTION
            );

        } catch (\PDOException $e) {
            echo "Error Connection: {$e->getMessage()}";
            die;
        }
    }
}

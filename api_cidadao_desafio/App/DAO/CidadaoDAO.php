<?php

namespace App\DAO;

use App\Models\Cidadao;
use App\Validations\Validate as VD;

class CidadaoDAO extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método para pegar os dados de um cidadão cadastrado no banco de dados.
     *
     * @param string $search
     * @return array
     */
    public function findOne(string $search): array
    {

        if (!VD::validateCPF($search)) return ['error' => 'CPF inválido'];

        $query = "SELECT 
        ci.nome as Nome,
        ci.sobrenome as Sobrenome,
        ci.cpf as Cpf,
        ci.email as Email,
        ci.telefone as Telefone,
        ci.cep as CEP,
        ci.logradouro as Logradouro,
        ci.bairro as Bairro,
        ci.localidade as Localidade,
        ci.uf as Estado
        FROM cidadao AS ci
        WHERE ci.cpf = :cpf
        ";

        $stmt = $this->pdo->prepare($query);
        $stmt->bindValue(':cpf', $search, \PDO::PARAM_STR);

        try {
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            if (!empty($result)) return $result;
            return ['mensagem' => "Cidadão não foi encontrado"];
        } catch (\PDOException $e) {
            echo "Error: {$e->getMessage()}";
            die;
        }
    }

    /**
     * Método para pegar os dados de todos cidadão cadastrado no banco de dados.
     *
     * @return array
     */
    public function findAll(): array
    {
        $query = "SELECT 
        ci.nome as Nome,
        ci.sobrenome as Sobrenome,
        ci.cpf as Cpf,
        ci.email as Email,
        ci.telefone as Telefone,
        ci.cep as CEP,
        ci.logradouro as Logradouro,
        ci.bairro as Bairro,
        ci.localidade as Localidade,
        ci.uf as UF
        FROM cidadao AS ci
        ORDER BY ci.nome
        ";

        $stmt = $this->pdo->prepare($query);

        try {
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if (!empty($result)) return $result;
            return ['mensagem' => "Nenhum cidadão cadastrado"];
        } catch (\PDOException $e) {
            echo "Error: {$e->getMessage()}";
            die;
        }
    }

    /**
     * Método para inserir um novo cidadão no banco de dados.
     *
     * @param Cidadao $cidadao
     * @return void
     */
    public function save(Cidadao $cidadao)
    {
        $query = "
        INSERT into cidadao(
            cpf, 
            nome, 
            sobrenome, 
            email, 
            telefone, 
            cep, 
            logradouro, 
            bairro, 
            localidade, 
            uf)
        VALUES (
            :cpf, 
            :nome, 
            :sobrenome, 
            :email, 
            :telefone, 
            :cep, 
            :logradouro, 
            :bairro, 
            :localidade, 
            :uf)";

        $stmt = $this->pdo->prepare($query);
        $stmt->bindValue(':cpf', $cidadao->__get('cpf'), \PDO::PARAM_STR);
        $stmt->bindValue(':nome', $cidadao->__get('nome'), \PDO::PARAM_STR);
        $stmt->bindValue(':sobrenome', $cidadao->__get('sobrenome'), \PDO::PARAM_STR);
        $stmt->bindValue(':email', $cidadao->__get('email'), \PDO::PARAM_STR);
        $stmt->bindValue(':telefone', $cidadao->__get('telefone'), \PDO::PARAM_STR);
        $stmt->bindValue(':cep', $cidadao->__get('cep'), \PDO::PARAM_STR);
        $stmt->bindValue(':logradouro', $cidadao->__get('logradouro'), \PDO::PARAM_STR);
        $stmt->bindValue(':bairro', $cidadao->__get('bairro'), \PDO::PARAM_STR);
        $stmt->bindValue(':localidade', $cidadao->__get('localidade'), \PDO::PARAM_STR);
        $stmt->bindValue(':uf', $cidadao->__get('uf'), \PDO::PARAM_STR);

        try {
            $stmt->execute();
            return [
                'mensagem' => "O cidadão {$cidadao->__get('nome')} {$cidadao->__get('sobrenome')} cadastrado com sucesso"
            ];
        } catch (\PDOException $e) {
            echo "Error: {$e->getMessage()}";
            die;
        }
    }

    /**
     * Método para atualizar um cidadão cadastrado no banco de dados.
     *
     * @param Cidadao $cidadao
     * @return void
     */
    public function update(Cidadao $cidadao)
    {
        $query = "UPDATE cidadao AS ci
        SET  
        ci.nome = :nome, 
        ci.sobrenome = :sobrenome, 
        ci.email = :email, 
        ci.telefone = :telefone, 
        ci.cep = :cep, 
        ci.logradouro = :logradouro, 
        ci.bairro = :bairro, 
        ci.localidade = :localidade, 
        ci.uf = :uf
        WHERE ci.cpf = :cpf";

        $stmt = $this->pdo->prepare($query);
        $stmt->bindValue(':cpf', $cidadao->__get('cpf'), \PDO::PARAM_STR);
        $stmt->bindValue(':nome', $cidadao->__get('nome'), \PDO::PARAM_STR);
        $stmt->bindValue(':sobrenome', $cidadao->__get('sobrenome'), \PDO::PARAM_STR);
        $stmt->bindValue(':email', $cidadao->__get('email'), \PDO::PARAM_STR);
        $stmt->bindValue(':telefone', $cidadao->__get('telefone'), \PDO::PARAM_STR);
        $stmt->bindValue(':cep', $cidadao->__get('cep'), \PDO::PARAM_STR);
        $stmt->bindValue(':logradouro', $cidadao->__get('logradouro'), \PDO::PARAM_STR);
        $stmt->bindValue(':bairro', $cidadao->__get('bairro'), \PDO::PARAM_STR);
        $stmt->bindValue(':localidade', $cidadao->__get('localidade'), \PDO::PARAM_STR);
        $stmt->bindValue(':uf', $cidadao->__get('uf'), \PDO::PARAM_STR);

        try {
            $stmt->execute();
            return [
                'mensagem' => "Dados do Cidadão {$cidadao->__get('nome')} {$cidadao->__get('sobrenome')} com sucesso"
            ];
        } catch (\PDOException $e) {
            echo "Error: {$e->getMessage()}";
            die;
        }
    }

    /**
     * Método para deletar um cidadão cadastrado no banco de dados.
     *
     * @param string $cpf
     * @param string $nome
     * @param string $sobrenome
     * @return void
     */
    public function delete(string $cpf, string $nome, string $sobrenome)
    {
        $query = "DELETE FROM cidadao WHERE cpf = :cpf";

        $stmt = $this->pdo->prepare($query);
        $stmt->bindValue(':cpf', $cpf, \PDO::PARAM_STR);

        try {
            $stmt->execute();
            return ['mensagem' => "O cidadão {$nome} {$sobrenome} foi deletado com sucesso"];
        } catch (\PDOException $e) {
            echo "Error: {$e->getMessage()}";
            die;
        }
    }
}

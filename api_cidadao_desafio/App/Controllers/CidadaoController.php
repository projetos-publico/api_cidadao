<?php

namespace App\Controllers;

use App\DAO\CidadaoDAO;
use App\Models\Cidadao;
use App\Validations\Validate as VD;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class CidadaoController
{

    /**
     * Método da rota para pegar os dados de um cidadão.
     * 
     * Se for passado um cpf ele retorna o dado do cidadão se estiver cadastrado.
     * Se não for passado nenhum cpf ele retorna todos os cidadãos cadastrados.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function getCitizen(Request $request, Response $response, array $args): Response
    {
        $data = $request->getParsedBody();

        $search = $data['cpf'] ?? false;
        $cidadaoDAO = new CidadaoDAO();

        if ($search) {
            $response = $response
                ->withHeader("Content-Type", "application/json;charset=utf-8")
                ->write(json_encode($cidadaoDAO->findOne($search), JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT |  JSON_UNESCAPED_UNICODE));
            return $response;
        }

        $response = $response
            ->withHeader("Content-Type", "application/json;charset=utf-8")
            ->write(json_encode($cidadaoDAO->findAll(), JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT |  JSON_UNESCAPED_UNICODE));
        return $response;
    }

    /**
     * Método da rota de cadastro de cidadão.
     * 
     * Espera um json ou formulário com os campos:
     * cpf, nome, sobrenome, email, telefone, cep. OBS:(todos como string)
     * 
     * O logradouro e preenchido automático com base no cep,
     * busca no ViaCEO: https://viacep.com.br/ws/01001000/json/
     * 
     * O cadastro do cidadão só é efetivado com a validação dos dados de todos os campos, 
     * nenhum campo pode estar vazio, o CPF tem que ser válido, CEP deve existir.
     * 
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function insertCitizen(Request $request, Response $response, array $args): Response
    {
        $data = $request->getParsedBody();

        // Checar se já esta cadastrado.
        if (isset($data['cpf'])) {
            if (!$this->checkCidadaoExist($data['cpf'])) {
                $response = $response
                    ->withHeader("Content-Type", "application/json;charset=utf-8")
                    ->write(
                        json_encode(
                            ['mensagem' => 'Esse cpf já esta cadastrado'],
                            JSON_UNESCAPED_SLASHES
                                | JSON_PRETTY_PRINT
                                |  JSON_UNESCAPED_UNICODE
                        )
                    );
                return $response;
            }
        }

        if (!empty($data) && count($data) == 6) {
            $validate = VD::validateData($data);

            if ($validate['isValid']) {
                // Estancia um cidadão
                $cidadao = new Cidadao($data);

                if (!$cidadao->getLogradouro()) {

                    $response = $response
                        ->withHeader("Content-Type", "application/json;charset=utf-8")
                        ->write(
                            json_encode(
                                [
                                    'error' => 'não foi possivel efetuar o cadastro',
                                    'endereço' => 'não encontrado, cep informado não existe'
                                ],
                                JSON_UNESCAPED_SLASHES
                                    | JSON_PRETTY_PRINT
                                    |  JSON_UNESCAPED_UNICODE
                            )
                        );
                    return $response;
                }

                $cidadaoDAO = new CidadaoDAO();
                
                $response = $response
                    ->withHeader("Content-Type", "application/json;charset=utf-8")
                    ->write(
                        json_encode(
                            $cidadaoDAO->save($cidadao),
                            JSON_UNESCAPED_SLASHES
                                | JSON_PRETTY_PRINT
                                |  JSON_UNESCAPED_UNICODE
                        )
                    );
                return $response;
            }

            
            $response = $response
                ->withHeader("Content-Type", "application/json;charset=utf-8")
                ->write(
                    json_encode(
                        $validate['error'],
                        JSON_UNESCAPED_SLASHES
                            | JSON_PRETTY_PRINT
                            |  JSON_UNESCAPED_UNICODE
                    )
                );
            return $response;
        }

        $response = $response
            ->withHeader("Content-Type", "application/json;charset=utf-8")
            ->write(
                json_encode(
                    [
                        'mensagem' => 'Para Cadastrar um novo cidadão é obrigatorio: informar:',
                        'campos' => 'cpf, nome, sobrenome, email, telefone e cep'
                    ],
                    JSON_UNESCAPED_SLASHES
                        | JSON_PRETTY_PRINT
                        |  JSON_UNESCAPED_UNICODE
                )
            );
        return $response;
    }


    /**
     * Método da rota para atualizar o cadastro de um cidadão.
     * 
     * Obrigatório informar um cpf para checar se o cidadão existe para conseguir 
     * atualizar os dados.
     * 
     * Campos que podem ser atualizado nome, sobrenome, email, telefone e cep,
     * só precisa passar o campo que deseja atualizar.
     * 
     * O cadastro do cidadão só é atualizado com a validação dos campos informado,
     * portanto os campos passados não podem estar vazios, para o cep tem que ser válido. 
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function updateCitizen(Request $request, Response $response, array $args): Response
    {
        $data = $request->getParsedBody();

        if (isset($data['cpf'])) {
            $cidadao = $this->getCidadao($data['cpf']);

            if (!empty($cidadao)) {
                
                $response = $response
                    ->withHeader("Content-Type", "application/json;charset=utf-8")
                    ->write(
                        json_encode(
                            $this->atualizaCidadao($data, new Cidadao($cidadao)),
                            JSON_UNESCAPED_SLASHES
                                | JSON_PRETTY_PRINT
                                |  JSON_UNESCAPED_UNICODE
                        )
                    );
                return $response;
            }

            $response = $response
                ->withHeader("Content-Type", "application/json;charset=utf-8")
                ->write(
                    json_encode(
                        [
                            'mensagem' => 'Nenhum cidadão cadastrado com esse cpf, para atualizar os dados'
                        ],
                        JSON_UNESCAPED_SLASHES
                            | JSON_PRETTY_PRINT
                            |  JSON_UNESCAPED_UNICODE
                    )
                );
            return $response;
        }

        $response = $response
            ->withHeader("Content-Type", "application/json;charset=utf-8")
            ->write(
                json_encode(
                    [
                        'error' => 'Para atualizar o cadastro de cidadão é obrigatorio informar cpf'
                    ],
                    JSON_UNESCAPED_SLASHES
                        | JSON_PRETTY_PRINT
                        |  JSON_UNESCAPED_UNICODE
                )
            );
        return $response;
    }

    /**
     * Método da rota para deletar um cidadão cadastrado.
     * 
     * Obrigatório informar o CPF para checar se o cidadão está cadastrado antes de deletar.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function deleteCitizen(Request $request, Response $response, array $args): Response
    {

        $data = $request->getParsedBody();

        if (isset($data['cpf'])) {
            if (!VD::validateCPF($data['cpf'])) {
                
                $response = $response
                    ->withHeader("Content-Type", "application/json;charset=utf-8")
                    ->write(
                        json_encode(
                            [
                                'error' => 'CPF, inválido'
                            ],
                            JSON_UNESCAPED_SLASHES
                                | JSON_PRETTY_PRINT
                                |  JSON_UNESCAPED_UNICODE
                        )
                    );
                return $response;
            }

            $cidadao = $this->getCidadao($data['cpf']);

            if (empty($cidadao)) {
                

                $response = $response
                    ->withHeader("Content-Type", "application/json;charset=utf-8")
                    ->write(
                        json_encode(
                            [
                                'mensagem' => 'Nenhum cidadão encontrado para deletar, com o cpf informado',
                            ],
                            JSON_UNESCAPED_SLASHES
                                | JSON_PRETTY_PRINT
                                |  JSON_UNESCAPED_UNICODE
                        )
                    );
                return $response;
            }

            $cidadaoDAO = new CidadaoDAO();

            $response = $response
                ->withHeader("Content-Type", "application/json;charset=utf-8")
                ->write(
                    json_encode(
                        $cidadaoDAO->delete($data['cpf'], $cidadao['Nome'], $cidadao['Sobrenome']),
                        JSON_UNESCAPED_SLASHES
                            | JSON_PRETTY_PRINT
                            |  JSON_UNESCAPED_UNICODE
                    )
                );
            return $response;
        }

        $response = $response
            ->withHeader("Content-Type", "application/json;charset=utf-8")
            ->write(
                json_encode(
                    [
                        'error' => 'Para deletar o cadastro do cidadão, é obrigatorio informar qual cpf deseja deletar',
                    ],
                    JSON_UNESCAPED_SLASHES
                        | JSON_PRETTY_PRINT
                        |  JSON_UNESCAPED_UNICODE
                )
            );
        return $response;
    }


    /**
     * Método privado da classe para checar se um cidadão existe informando como 
     * obrigatoriedade o CPF.
     * 
     * Se o CPF informado existir retorna falso e não permite cadastrar o mesmo CPF.
     * 
     * Se o CPF informado não existir retorna verdadeiro e permite cadastrar o cidadão.
     * 
     * @param string $cpf
     * @return boolean
     */
    private function checkCidadaoExist(string $cpf): bool
    {
        $cidadaoDAO = new CidadaoDAO();
        $cidadao = $cidadaoDAO->findOne($cpf);

        if (isset($cidadao['mensagem']) || isset($cidadao['error'])) return true;

        return false;
    }


    /**
     * Método para pegar os dados de um cidadão cadastrado.
     *
     * Retorna um array vazio caso o cidadão não esteja cadastrado.
     * Caso o cidadão já estiver cadastrado retorna seus dados.
     * 
     * @param string $cpf
     * @return array
     */
    private function getCidadao(string $cpf): array
    {
        $cidadaoDAO = new CidadaoDAO();
        $cidadao = $cidadaoDAO->findOne($cpf);
        if (isset($cidadao['mensagem']) || isset($cidadao['Error'])) return [];

        return $cidadao;
    }


    /**
     * Método para validar e preparar os dados para atualização de um cidadão.
     * 
     * 
     * @param array $data
     * @param Cidadao $cidadao
     * @return array
     */
    private function atualizaCidadao(array $data, Cidadao $cidadao): array
    {
        if (!empty($data) && count($data) <= 6) {
            $validate = VD::validateData($data);

            if ($validate['isValid']) {
                $cidadao->atualizaInformacao($data);

                if (!$cidadao->getLogradouro()) {
                    return [
                        'error' => 'Falha em atualizar os dados do cidadão',
                        'endereço' => 'não encontrado, cep informado não existe'
                    ];
                }

                $cidadaoDAO = new CidadaoDAO();
                return $cidadaoDAO->update($cidadao);
            }

            return $validate['error'];
        }

        return [
            'error' => 'Não foi possivel atualizar o cadastro',
            'obrigatório' => 'Informar o cpf',
            'campos' => 'Os que podem ser atualizado: nome, sobrenome, email, telefone e cep'
        ];
    }
}

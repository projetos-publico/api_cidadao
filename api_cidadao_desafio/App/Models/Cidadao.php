<?php

namespace App\Models;

final class Cidadao
{

    private int $id;
    private string $nome;
    private string $sobrenome;
    private string $cpf;
    private string $email;
    private string $telefone;
    private string $cep;
    private string $logradouro;
    private string $bairro;
    private string $localidade;
    private string $uf;

    public function __construct(array $cidadao)
    {
        foreach ($cidadao as $key => $value) {
            $this->__set(strtolower($key), $value);
        }
    }

    public function __set($attr, $value)
    {
        $this->$attr = $value;
    }

    public function __get($attr)
    {
        return $this->$attr;
    }

    /**
     * Método responsável por verificar se o cep informado é válido e definir os dados.
     *
     * Usado ViaCEP para obter os dados do cep.
     * 
     * @return boolean
     */
    public function getLogradouro(): bool
    {
        $cep = str_replace('-', '', $this->cep);
        $logradouro = file_get_contents('https://viacep.com.br/ws/' . $cep . '/json/');
        $logradouro = json_decode($logradouro, true);

        if (isset($logradouro['erro']) && $logradouro['erro']) {
            return false;
        }

        foreach ($logradouro as $key => $value) {
            if ($key === 'logradouro' || $key === 'bairro' || $key === 'localidade' || $key === 'uf') {
                $this->__set($key, $value);
            }
        }

        return true;
    }


    /**
     * Método que define quais informações vão ser atualizadas.
     *
     * @param array $data
     * @return void
     */
    public function atualizaInformacao(array $data)
    {
        foreach ($data as $key => $value) {
            $this->__set(strtolower($key), $value);
        }
    }
}

<?php

namespace App\Validations;

final class Validate
{
    /**
     * Verificar se um cpf é válido. 
     * Retornando true para valido e false para invalido.
     *
     * @param string $cpf
     * @return boolean
     */
    public static function validateCPF(string $cpf): bool
    {
        $cpf = preg_replace('/[^0-9]/', '', $cpf);
        if (strlen($cpf) !== 11 || preg_match('/([0-9])\1{10}/', $cpf)) {
            return false;
        }
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validar os campos informados para cadastro.
     *
     * @param array $data
     * @return array
     */
    public static function validateData(array $data): array
    {
        $validate = true;

        // Checa se todos os campos informados são os esperados.
        $notExpectedFields = self::checkExpectedFields($data);
        if (!empty($notExpectedFields)) return ['isValid' => false, 'error' => $notExpectedFields];

        // Checa se algum campo está inválido ou vazio.
        $dataCheck = self::checkInputs($data);
        if (!empty($dataCheck)) return ['isValid' => false, 'error' => $dataCheck];

        return ['isValid' => $validate];
    }

    /**
     * Validar os campos enviados.
     *  CPF -> verifica se é válido ou inválido.
     *  Os demais campos checam se está vazio.
     * 
     * @param array $data
     * @return array
     */
    private static function checkInputs(array $data): array
    {
        $dataCheck = [];
        foreach ($data as $key => $value) {
            if ($key === 'cpf') {
                if (!self::validateCPF($value)) $dataCheck[$key] = 'Status: Inválido';
                continue;
            }
            if($key === 'cep'){
                if(!self::validateCEP($value)) $dataCheck[$key] = 'Status: Inválido';
                continue;
            }
            if (empty($value)) {
                $dataCheck[$key] = 'Status: vazio';
                continue;
            }
        }
        return $dataCheck;
    }

    /**
     * Valida se todos os campos informados são esperados.
     *
     * @param array $data
     * @return array
     */
    private static function checkExpectedFields(array $data): array
    {
        $notExpectedFields = [];
        $expectedFields = ['cep', 'cpf', 'nome', 'sobrenome', 'email', 'telefone'];
        foreach ($data as $key => $value) {
            if (!in_array($key, $expectedFields)) $notExpectedFields[$key] = 'Error: Campo não esperado';
        }
        return $notExpectedFields;
    }

    /**
     * Validar um cep 
     *
     * @param string $cep
     * @return boolean
     */
    private static function validateCEP(string $cep): bool
    {
        $cep = str_replace('-', '', $cep);
        return strlen($cep) === 8;
    }
}

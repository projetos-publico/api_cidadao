<?php
/**
 * Em produção deixar essa opção com false.
 * Em dev deixar essa opção com True.
 * 
 * true -> mostra o erros do framework de forma amigavel
 * false -> não mostra os erros na tela.
 * 
 */
putenv('DISPLAY_ERRORS_DETAILS' . false); 


// Configuração do Banco de Dados
putenv('DB_CONFIG_HOST='); // Host do banco de dados
putenv('DB_CONFIG_DBNAME='); // Nome do banco de dados
putenv('DB_CONFIG_USER='); // Usuário de acesso
putenv('DB_CONFIG_PASSWORD='); // Senha do usuário
putenv('DB_CONFIG_PORT='); // Porta do banco de dados
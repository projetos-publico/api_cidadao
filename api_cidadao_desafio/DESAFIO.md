## Projeto:

Desenvolvimento de API em PHP para manipular dados de cidadão.
 
## Requisitos:

- Permitir inserir, atualizar e deletar dados de cidadão com: nome, sobrenome, cpf, contatos (email e celular), endereço (cep, logradouro, bairro, cidade e uf); :heavy_check_mark:
- Permitir consultar todos os cidadãos em ordem alfabética crescente (sem filtro); :heavy_check_mark:
- Permitir consultar um cidadão pelo CPF; :heavy_check_mark:
- Não permitir cadastrar cidadão com o mesmo CPF; :heavy_check_mark:
- Permitir que a inserção também seja feita através da Linha de Comando; :heavy_check_mark:
- Com o CEP as informações de logradouro, bairro, cidade e uf devem ser buscadas no ViaCEP: https://viacep.com.br/ws/01001000/json/ :heavy_check_mark:
 
## Considerações:

- A API pode ser REST ou GraphQL (Trafegar JSON); :heavy_check_mark: Rest
- A escolha do armazenamento dos dados e a utilização (ou não) de frameworks ficam a critério do avaliado. Descrever o porquê da decisão; :heavy_check_mark: SlimFramework, MariaDB
- Incluir documentação de como utilizar a API;
- Incluir testes (opcional);
 
## O que será avaliado?

- Qualidade de código e conhecimento em OOP;
- Boas práticas de desenvolvimento;
- Capacidade de solução de problemas;


## Envio

Aplicação pode ser enviado via link do Bitbucket.

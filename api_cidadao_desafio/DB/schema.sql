CREATE DATABASE api_cidadao; 

grant all privileges on api_cidadao.* to 'dev' identified by 'dev@2021';

use api_cidadao;


create table if not exists `cidadao`(
    `id` int not null auto_increment,
    `nome` varchar(255) not null,
    `sobrenome` varchar(255) not null,
    `cpf` varchar(14) not null unique,
    `telefone` varchar(30) not null,
    `email` varchar(255) not null,
    `cep` varchar(9) not null,
    `logradouro` varchar(255) not null,
    `bairro` varchar(255) not null,
    `localidade` varchar(255) not null,
    `uf` varchar(2) not null,
    primary key (id)
);

<?php

use App\Controllers\CidadaoController;

use function config\slimConfiguration;

$app = new \Slim\App(slimConfiguration());

// ====================================================

$app->get('/cidadao[/{search}]', CidadaoController::class . ':getCitizen');
$app->post('/cidadao', CidadaoController::class . ':insertCitizen');
$app->put('/cidadao', CidadaoController::class . ':updateCitizen');
$app->delete('/cidadao', CidadaoController::class . ':deleteCitizen');

// ====================================================

$app->run();